angular.module("myApp")
.controller("countyCtrl", ["$scope", "$rootScope", "$state", "countyService", "cityService",  
	function($scope, $rootScope, $state, countyService, cityService){
	var countyCtrl = this;
	
	countyCtrl.selectedCounty = "";
	countyCtrl.cities = [];
	
	if($rootScope.counties.length <= 0){
		countyService.getCounties().then(function(data){
			$rootScope.counties = data;
		});
	}
	
	$scope.$watch(
	function(){
		return countyCtrl.selectedCounty
	}, function(){
		if(countyCtrl.selectedCounty){
			cityService.citiesByCountyId(countyCtrl.selectedCounty)
				.then(function(data){
					countyCtrl.cities = data;
				});
		}
	});
	
	countyCtrl.newCity = function(){
		$state.go('city', {
			county_id: countyCtrl.selectedCounty
		});
	};
	
	countyCtrl.editCity = function(city_id, city_name){
		$state.go('city', {
			city_id: city_id,
			city_name: city_name
		});
	};
	
	countyCtrl.deleteCity = function(city_id){
		cityService.deleteCity(city_id);
		cityService.citiesByCountyId(countyCtrl.selectedCounty)
			.then(function(data){
				countyCtrl.cities = data;
			});
	};
	
}]);