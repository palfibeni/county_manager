angular.module("myApp")
.controller("cityCtrl", ["$scope", "$state", "$stateParams", "cityService",  function($scope, $state, $stateParams, cityService){
	var cityCtrl = this;
	
	cityCtrl.city = {
		id: $stateParams.city_id,
		county_id: $stateParams.county_id,
		name: $stateParams.city_name
	};
	
	cityCtrl.sendRequest = function(){
		if(cityCtrl.city.id){
			cityService.editCity(cityCtrl.city.id, cityCtrl.city.name);
		}else{
			cityService.addCity(cityCtrl.city.county_id, cityCtrl.city.name);
		}
		cityCtrl.return();
	};

	cityCtrl.return = function(){
		$state.go('county');
	}
}]);