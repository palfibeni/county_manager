<?php
	switch($headers['RestFunc']){
		case 'cityByCountyId':
			$query = "SELECT * FROM city WHERE county_id = " . $_REQUEST['county_id'];
			$result = mysqli_query($connection, $query);
				
			if($result == false) {
				// Handle failure - log the error, notify administrator, etc.
			} else {
				// Fetch all the rows in an array
				$rows = array();
				while ($row = mysqli_fetch_assoc($result)) {
					$rows[] = $row;
				}
				echo json_encode($rows);
			}
			$result->close();
			break;
		case 'cityNew':
			$content = trim(file_get_contents("php://input"));
			$city =  (json_decode($content));

			$query = "INSERT INTO city (
				county_id, 
				name)
			VALUES ('". 
				$city->{'county_id'} ."', '". 
				$city->{'name'} ."')";
			$result = mysqli_query($connection, $query);
				
			if($result == false) {
				// Handle failure - log the error, notify administrator, etc.
			} else {
				echo 'success';
			}
			break;
		case 'cityEdit':
			$content = trim(file_get_contents("php://input"));
			$city =  (json_decode($content));

			$query = "UPDATE city SET name='". $city->{'name'} ."' WHERE id =".$city->{'id'};
			$result = mysqli_query($connection, $query);
				
			if($result == false) {
				// Handle failure - log the error, notify administrator, etc.
			} else {
				echo 'success';
			}
			break;
		case 'cityDelete':
			$query = "DELETE FROM city WHERE id = " . $_REQUEST['city_id'];
			$result = mysqli_query($connection, $query);
				
			if($result == false) {
				// Handle failure - log the error, notify administrator, etc.
			} else {
				echo 'success';
			}
			break;
	}
?>