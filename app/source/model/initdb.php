<?php
	static $connection;
	
	if(!isset($connection)) {
		$connection = mysqli_connect(config::host, config::user, config::pass, config::dbnm);
	}
	if (!$connection) {
		echo "Error: Unable to connect to MySQL." . PHP_EOL;
		echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}
	mysqli_set_charset($connection, "utf8");

?>
