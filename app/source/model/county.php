<?php
	switch($headers['RestFunc']){
		case 'countyAll':
			$query = "SELECT * FROM county";
			$result = mysqli_query($connection, $query);
				
			if($result == false) {
				// Handle failure - log the error, notify administrator, etc.
			} else {
				// Fetch all the rows in an array
				$rows = array();
				while ($row = mysqli_fetch_assoc($result)) {
					$rows[] = $row;
				}
				echo json_encode($rows);
			}
			$result->close();
			break;
	}
?>