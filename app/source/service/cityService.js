angular.module("myApp")
.service("cityService", ["$http", "$q", function($http, $q){
	cityService = this;
	
	cityService.citiesByCountyId = function(county_id){
		var defferer = $q.defer();
		$http.defaults.headers.common.RestFunc = 'cityByCountyId';
		$http.get('source/model/core.php', { params: {county_id: county_id}})
			.then(function(response){
				defferer.resolve(response.data);
			});
		return defferer.promise;
	};
	
	cityService.addCity = function(county_id, city_name){
		$http.defaults.headers.common.RestFunc = 'cityNew';
		$http.post('source/model/core.php', JSON.stringify({county_id: county_id, name: city_name}))
			.then(function(){
				
			});
	}
	
	cityService.editCity = function(city_id, city_name){
		$http.defaults.headers.common.RestFunc = 'cityEdit';
		$http.post('source/model/core.php', JSON.stringify({id: city_id, name: city_name}))
			.then(function(){
				
			});
	}
	
	cityService.deleteCity = function(city_id){
		$http.defaults.headers.common.RestFunc = 'cityDelete';
		$http.delete('source/model/core.php', {params: {city_id: city_id}})
			.then(function(){
				
			});
	}
}]);