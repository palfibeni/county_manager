angular.module("myApp")
.service("countyService", ["$http", "$q", function($http, $q){
	countyService = this;
	
	countyService.getCounties = function(){
		var defferer = $q.defer();
		$http.defaults.headers.common.RestFunc = 'countyAll';
		$http.get('source/model/core.php')
			.then(function(response){
				defferer.resolve(response.data);
			});
		return defferer.promise;
	};
}]);