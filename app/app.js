'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ui.router'
]).config(['$locationProvider', '$stateProvider', function($locationProvider, $stateProvider) {
	// $locationProvider.hashPrefix('!');

	$stateProvider.state('main', {
		url: '/',
		templateUrl: 'source/view/main.html'
	}).state('county', {
		url: '/county',
		templateUrl: 'source/view/county.html'
	 }).state('city', {	
		url: '/city',
		templateUrl: 'source/view/city.html',
		params: {
			city_id: null,
			county_id: null,
			city_name: ""
		}
	});
}]).run(function($rootScope) {
	$rootScope.counties = [];
});
